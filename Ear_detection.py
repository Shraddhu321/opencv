import cv2
import numpy as np

leftear_cascade = cv2.CascadeClassifier('haarcascade_leftear.xml')
rightear_cascade = cv2.CascadeClassifier('haarcascade_rightear.xml')

if leftear_cascade.empty():
  raise IOError('Unable to load the left ear cascade classifier xml file')

if rightear_cascade.empty():
  raise IOError('Unable to load the right ear cascade classifier xml file')

img = cv2.imread('/content/sample_data/images.jpg')

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

leftear = leftear_cascade.detectMultiScale(gray, 1.3, 5)
rightear = rightear_cascade.detectMultiScale(gray, 1.3, 5)

for (x,y,w,h) in leftear:
    cv2.rectangle(img, (x,y), (x+w,y+h), (0,255,0), 3)

for (x,y,w,h) in rightear:
    cv2.rectangle(img, (x,y), (x+w,y+h), (255,0,0), 3)

cv2.imshow('Ear Detector', img)
cv2.waitKey()
cv2.destroyAllWindows()
#If you run the above code on an image, you should see something...


